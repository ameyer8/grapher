#!/usr/bin/ruby

class Grapher
end

h = 40
w = 80

class Array
  def t_min
    self.sort.first
  end
  def t_max
    self.sort.last
  end
end

class Point
  attr_accessor :x, :y
  def initialize(x=0,y=0)
    @x = x
    @y = y
  end
  def to_s
    puts "[ #{x}, #{y} ]"
  end
end


def print_graph(g)
  g.each{ |line|
    line.each{ |ch|
      printf("%c", ch)
    }
    printf("\n")
  }
end

def clear_graph(g)
  g.each_index{ |l_i|
    if l_i > g.length - 4 - 1
    else
      g[l_i].each_index { |ch_i|
        if ch_i < 7 + 1
        else
          g[l_i][ch_i] = " "
        end
      }
    end
  }
end

def update_axis(g,high,low,axis, max, min)
  if axis == :y
    step = (max - min)/(5.0*low.y-high.y)
#    puts "step: #{step}"
    i = 0
    (low.y - high.y)/5.times{
      g[high.y + i][2] = max
      max += step
      i += 5

    }

  else

  end
end

graph = []

h.times {
  graph << Array.new(w,".")
}

graph[h-4].map!{ |ch| ch = "_"}
graph.each{ |l| l[7] = "|"}

clear_graph(graph)

#xdata = [0,1,2,3,4,5,6,7,8,9]
xdata = (1..60).to_a.map!{ |x| x=x.to_f/10 }
#ydata = [100,85,65,90,45,25,30,45,89,67]
ydata = xdata.map{|x| Math.sin(x)}

low = Point.new(7, h - 4)
high = Point.new(w - 1, 1)


yd_min = ydata.min
yd_max = ydata.max

xd_min = xdata.min
xd_max = xdata.max

ydata.each_index { |x|
  yi = low.y - ((ydata[x].to_f-yd_min)/(yd_max - yd_min) * (low.y-high.y)).floor
  xi = low.x - ((xdata[x].to_f-xd_min)/xd_max * (low.x-high.x)).floor
  puts "#{xi}, #{yi}"
  graph[yi][xi] = '*'
}

#update_axis(graph,high,low,:y,yd_max,yd_min)

print_graph(graph)





